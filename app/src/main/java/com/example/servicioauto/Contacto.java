package com.example.servicioauto;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Contacto extends AppCompatActivity {

        Button btn;
        final private int MY_PERMISSIONS_REQUEST_CALL_PHONE = 10;
        int hasreadcallpermission;

        public void accessPermision() {

            hasreadcallpermission = checkSelfPermission(Manifest.permission.CALL_PHONE);
            if (hasreadcallpermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_CALL_PHONE);
            }
        }


        @Override
        public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // PERMISO CONCEDIDO, procede a realizar lo que tienes que hacer
                        Toast.makeText(Contacto.this, "Permiso  concedido ", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:112"));
                    } else {
                        Toast.makeText(Contacto.this, "Permiso  denegado ", Toast.LENGTH_SHORT).show();

                        // PERMISO DENEGADO
                    }
                    break;


                }
            }
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_contacto);

            btn = (Button) findViewById(R.id.btncontactar);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:0939820201"));
                    if (ActivityCompat.checkSelfPermission(Contacto.this, Manifest.permission.CALL_PHONE) !=
                            PackageManager.PERMISSION_GRANTED)

                        return;
                    startActivity(i);
                }
            });

            accessPermision();
        }
    }

